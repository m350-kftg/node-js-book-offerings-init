# URL für Offering API
@offeringsUrl = http://localhost:3001/api/offerings

# Alle TODOs abfragen
GET {{offeringsUrl}}

###

# Neues TODO hinzufügen
POST {{offeringsUrl}}
Content-Type: application/json

{
    "vendor": "exlibirs.ch",
    "price": 44
}

###

# Verändern von existierendem TODO
PUT {{offeringsUrl}}/3
Content-Type: application/json

{
    "vendor": "fontis.ch",
    "price": 45
}

###

# 404 Error: Verändern von TODO mit nicht existierender Id
PUT {{offeringsUrl}}/1000
Content-Type: application/json

{
    "vendor": "exlibirs.ch",
    "price": 44
}

###

# 400 Error: Verändern von TODO mit ungültiger Id
PUT {{offeringsUrl}}/a
Content-Type: application/json

{
    "vendor": "exlibirs.ch",
    "price": 44
}

###

# Löschen von existierendem TODO
DELETE {{offeringsUrl}}/4

###

# 404 Error: Verändern von TODO mit nicht existierender Id
DELETE {{offeringsUrl}}/1000

###

# 400 Error: Verändern von TODO mit ungültiger Id
DELETE {{offeringsUrl}}/a
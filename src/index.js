import express from 'express';
import path, { dirname } from 'path';
import { fileURLToPath } from 'url';
import { router as offeringRouter } from './backend/offering/offering.routes.js';

const app = express();

const __dirname = dirname(fileURLToPath(import.meta.url));
app.use(express.static(path.join(__dirname, 'frontend')));

app.use(express.json());
app.use('/api/offerings', offeringRouter);

app.listen(3001, () => {
  console.log('Server listens to http://localhost:3001');
});
